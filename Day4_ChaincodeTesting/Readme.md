**Chain Code Tesing**

Create a new folder named test

```
mkdir test
```

Now create a file named car-contract_test.go inside test directory
```
touch test/car-contract_test.go
```

We need to create mocks for all the method for fabric communication
```
go install github.com/maxbrunsfeld/counterfeiter/v6@latest
```
Or
```
git clone https://github.com/maxbrunsfeld/counterfeiter.git
cd counterfeiter
go build
sudo cp counterfeiter /usr/local/bin
```

The use the following command, to generate mocks
```
go generate
```
