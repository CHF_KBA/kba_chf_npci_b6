## How to Run
To run the continer using docker compose
```
docker compose up
```
This works if your are in the same folder and the docker-compose.yaml and the name is as is, otherwise

```
docker compose -f [path/to/compose/file] up
```

Now if you want to run it with detahced logs use `-d` flag.
```
docker compose -f [path/to/compose/file] up -d
```

To stop and remove, use
```
docker compose -f [path/to/compose/file] down
```
Which removes the network and containers

To remove the volumes use
```
docker-compose -f [path/to/compose/file] down -v
```