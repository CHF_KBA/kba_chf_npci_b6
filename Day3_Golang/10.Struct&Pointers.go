package main

import "fmt"

type gCar struct {
	mpl     int
	pertrol int
}

// Using pointer receiver for gCar's method
func (c *gCar) travelLeft() int {
	return c.mpl * c.pertrol
}

type eCar struct {
	mkwh int
	kwh  int
}

// Using pointer receiver for eCar's method
func (c *eCar) travelLeft() int {
	return c.mkwh * c.kwh
}

type engine interface {
	travelLeft() int
}

func travelPossible(mycar2 engine, miles int) {
	// Note: This will print the address of the interface value, not necessarily the address of the original struct.
	fmt.Printf("\nLocation in func %p", mycar2)
	if miles <= mycar2.travelLeft() {
		fmt.Println("\nYou can make it there")
	} else {
		fmt.Println("\nYou won't make it")
	}
}

func main10() {
	var mycar1 gCar = gCar{mpl: 23, pertrol: 23}
	fmt.Printf("\nLocation in main func %p", &mycar1)
	travelPossible(&mycar1, 530) // Passing a pointer

	var mycar2 eCar = eCar{mkwh: 23, kwh: 23}
	fmt.Printf("\nLocation in main func %p", &mycar2)
	travelPossible(&mycar2, 200) // Passing a pointer
}
