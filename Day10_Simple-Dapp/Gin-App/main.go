package main

import (
    "net/http"
    "sync"

    "github.com/gin-gonic/gin"
)

// A thread-safe map to store values.
type Store struct {
    sync.RWMutex
    Data map[string]string
}

var store = Store{
    Data: make(map[string]string),
}

func main() {
    r := gin.Default()

	// r.Static("/public", "./public")
	r.LoadHTMLGlob("templates/*")
	
	r.GET("/", func(c *gin.Context) {
        c.HTML(200, "index.html", gin.H{
            "title": "Gin App",
        })
	})


    r.POST("/store", func(c *gin.Context) {
        var json map[string]string
        if err := c.BindJSON(&json); err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        store.Lock()
        for key, value := range json {
            store.Data[key] = value
        }
        store.Unlock()

        c.JSON(http.StatusOK, gin.H{"status": "ok"})
    })

    r.GET("/retrieve/:key", func(c *gin.Context) {
        key := c.Param("key")

        store.RLock()
        value, exists := store.Data[key]
        store.RUnlock()

        if !exists {
            c.JSON(http.StatusNotFound, gin.H{"error": "Key not found"})
            return
        }

        c.JSON(http.StatusOK, gin.H{key: value})
    })

    r.Run("127.0.0.1:3000") // Listen and serve on 0.0.0.0:3000
}