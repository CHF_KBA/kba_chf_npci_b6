# Some Common Linux commands

### Note: Commands are case sensitive

To view the content of a folder
```
ls
```
To see hidden files
```
ls -a
```
To see a detailed list of files
```
ls -lh
```
To get the path of the current working directory
```
pwd
```
To create a directory
```
mkdir [directory name]
```
To create multiple directories
```
mkdir {test1,test2,test3}
```
To create parent and child directories
```
mkdir -p KBA/CHF
```
To copy a file use 
```
cp [source] [destination]
```
In the case of a directory use the -r flag
```
cp -r [source1, source2] [destination]
```
Now to move a file and folder use
```
mv [source] [destination]
```
To rename, the same command but give a different name at the destination, same for directory or file
```
mv testing/ testings
```
To delete
```
rm [file_name]
```
To delete folder
```
rm -r [fodler_name]
```
Use `-f` to force delete

To create a file use
```
touch file_name
```
To create and edit, use
```
nano file_name
```
Or 
```
gedit file_name
```
To print a message or a environment variable use
```
echo $PATH
```
To read a file name
```
cat [file name]
```
To get system details use
```
uname -a
whoami
lshw
```
To compress files into archive
```
tar -cvf [archive name] [files seperated by space]
```
To uncompress archive
```
tar -xvf [archive name]
```
To search for something in the output of another command
```
<Any command with output> | grep "<string to find>"
```
To list process running on the system
```
ps 
```
To list all process use the flags 'a', 'u', and 'x' respectively stand for 'all users', 'detailed user format', and 'also show processes not attached to a terminal'.
```
ps aux
```
To kill a process 
```
kill [process_id]
```
The chmod and chown commands, for changing file permissions and ownerships
```
chmod <parameter> filename
```
```
chown user:group filename
```
To download a file use 
```
wget [url]
```
To know process location or which process we using for a command
```
whereis go
which go
```
For system monitor 
```
top
```
```
htop
```
To see RAM usage
```
free -m
```
To see list of ports currently used
```
ss -tuln | grep :80
```
To get specific port details
```
lsof -i :80
```