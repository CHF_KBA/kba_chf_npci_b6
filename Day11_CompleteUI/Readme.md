/////Open a command terminal with in **KBA-Automobile** folder

`cd Chaincode`

`go mod tidy`

`cd Automobile-network/`

`./startAutomobileNetwork.sh`

`cd ../Auto-App`

`go mod tidy`

`go run .`

Browse http://localhost:8080/

Please explore all the features and capabilities by logging in.

//To stop

`ctrl+c`

`cd ../Automobile-network/`

`./stopAutomobileNetwork.sh`

