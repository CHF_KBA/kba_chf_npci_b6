# KBA_CHF_NPCI_B6

Elearning course link: https://elearning.kba.ai/courses/course-v1:KBA+CHF_GO+2023_Q3/course/

Hyperledger Fabric readthedocs: https://hyperledger-fabric.readthedocs.io/en/release-2.5/index.html

Fabric samples: https://github.com/hyperledger/fabric-samples

Hyperledger Fabirc github: https://github.com/hyperledger/fabric

Hyperledger: https://www.hyperledger.org/

Hyperledger India Chapter: https://wiki.hyperledger.org/display/HIRC/Hyperledger+India+Regional+Chapter+Home

Hyperledger Fabric Certified Practitioner (HFCP): https://training.linuxfoundation.org/certification/hyperledger-fabric-certified-practitioner-hfcp/

**Final Feedback:** https://forms.gle/xzdJE49yEZtq9SSE6

Note: **Project guidelines** uploaded in **Assignments** folder
