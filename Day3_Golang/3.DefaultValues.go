package main

import "fmt"

func main3() {
	var b byte
	var r rune
	var c64 complex64
	var c128 complex128

	fmt.Println("byte:", b)            // Outputs: byte: 0
	fmt.Println("rune:", r)            // Outputs: rune: 0
	fmt.Println("complex64:", c64)     // Outputs: complex64: (0+0i)
	fmt.Println("complex128:", c128)   // Outputs: complex128: (0+0i)
}