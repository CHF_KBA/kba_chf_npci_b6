package main

import "fmt"

type gasCar struct {
	mpl int
	pertrol int
}

func(c gasCar) travelLeftin() int {
	return c.mpl * c.pertrol
}

func istravelPossible(mycar2 *gasCar, miles int) {
	fmt.Printf("\nLocation in func %p", mycar2)
	if miles <= mycar2.travelLeftin() {
		fmt.Println("\nYou can make it there")
	} else {
		fmt.Println("\nYou wont make it")
	}
}

func main9() {
	var mycar1 gasCar = gasCar{mpl: 23, pertrol: 23}
	fmt.Printf("\nLocation in main func %p", &mycar1)
	istravelPossible(&mycar1, 530)
}