package contracts

// first write collection file

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/hyperledger/fabric-chaincode-go/shim"
)

// OrderContract contract for managing CRUD for Order
type OrderContract struct {
	contractapi.Contract
}

type Order struct { 
	AssetType  string `json:"assetType"`
	Color      string `json:"color"`
	DealerName string `json:"dealerName"`
	Make       string `json:"make"`
	Model      string `json:"model"`
	OrderID    string `json:"orderID"`
}

const collectionName string = "OrderCollection"


// OrderExists returns true when asset with given ID exists in private data collection
func (o *OrderContract) OrderExists(ctx contractapi.TransactionContextInterface, orderID string) (bool, error) {

	data, err := ctx.GetStub().GetPrivateDataHash(collectionName, orderID)

	if err != nil {
		return false, err
	}

	return data != nil, nil
}

// CreateOrder creates a new instance of Order
func (o *OrderContract) CreateOrder(ctx contractapi.TransactionContextInterface, orderID string) (string, error) {

	clientOrgID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return "", err
	}

	if clientOrgID == "Org2MSP" {
		exists, err := o.OrderExists(ctx, orderID)
		if err != nil {
			return "", fmt.Errorf("could not read from world state. %s", err)
		} else if exists {
			return "", fmt.Errorf("the asset %s already exists", orderID)
		}

		order := new(Order)

		transientData, err := ctx.GetStub().GetTransient()
		if err != nil {
			return "", err
		}

		if len(transientData) == 0 {
			return "", fmt.Errorf("Please provide the private data of make, model, color, dealerName")
		}

		make, exists := transientData["make"]
		if !exists {
			return "", fmt.Errorf("The make was not specified in transient data. Please try again")
		}
		order.Make = string(make)

		model, exists := transientData["model"]
		if !exists {
			return "", fmt.Errorf("The model was not specified in transient data. Please try again")
		}
		order.Model = string(model)

		color, exists := transientData["color"]
		if !exists {
			return "", fmt.Errorf("The color was not specified in transient data. Please try again")
		}
		order.Color = string(color)

		dealerName, exists := transientData["dealerName"]
		if !exists {
			return "", fmt.Errorf("the dealer was not specified in transient data. Please try again")
		}
		order.DealerName = string(dealerName)

		order.AssetType = "Order"
		order.OrderID = orderID

		bytes, _ := json.Marshal(order)
		err = ctx.GetStub().PutPrivateData(collectionName, orderID, bytes)
		if(err != nil) {
			return "", fmt.Errorf("could not able to write the data")
		}
		return fmt.Sprintf("Order with id %v added successfully", orderID), nil
	} else {
		return fmt.Sprintf("Order cannot be created by organisation with MSPID %v ", clientOrgID), nil
	}
}

// ReadOrder retrieves an instance of Order from the private data collection
func (o *OrderContract) ReadOrder(ctx contractapi.TransactionContextInterface, orderID string) (*Order, error) {
	exists, err := o.OrderExists(ctx, orderID)
	if err != nil {
		return nil, fmt.Errorf("Could not read from world state. %s", err)
	} else if !exists {
		return nil, fmt.Errorf("The asset %s does not exist", orderID)
	}

	bytes, err := ctx.GetStub().GetPrivateData(collectionName, orderID)
	if err != nil {
		return nil, err
	}
	order := new(Order)

	err = json.Unmarshal(bytes, order)

	if err != nil {
		return nil, fmt.Errorf("Could not unmarshal private data collection data to type Order")
	}

	return order, nil

}

// DeleteOrder deletes an instance of Order from the private data collection
func (o *OrderContract) DeleteOrder(ctx contractapi.TransactionContextInterface, orderID string) error {
	clientOrgID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return err
	}
	if clientOrgID == "Org2MSP" {
		exists, err := o.OrderExists(ctx, orderID)

		if err != nil {
			return fmt.Errorf("could not read from world state. %s", err)
		} else if !exists {
			return fmt.Errorf("the asset %s does not exist", orderID)
		}

		return ctx.GetStub().DelPrivateData(collectionName, orderID)
	} else {
		return fmt.Errorf("organisation with %v cannot delete the order", clientOrgID)
	}
}