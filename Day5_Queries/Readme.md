**Re-Deploy**
```
./network.sh deployCC -ccn KBA-Automobile -ccp ../../KBA-Automobile/Chaincode/ -ccl go -c autochannel -ccv 2.0 -ccs 3 -cccg ../../KBA-Automobile/Chaincode/collection.json 
```

**General Environment variables**

```
export FABRIC_CFG_PATH=$PWD/../config/
export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export ORG1_PEER_TLSROOTCERT=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export ORG2_PEER_TLSROOTCERT=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_TLS_ENABLED=true
```

**Environment variables for Org1**

```
export CORE_PEER_LOCALMSPID=Org1MSP
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
```

**Environment variables for Org2**
```
export CORE_PEER_LOCALMSPID=Org2MSP
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
```

**Environment variables for Org3**
```
export CORE_PEER_LOCALMSPID=Org3MSP
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org3.example.com/users/Admin@org3.example.com/msp
export CORE_PEER_ADDRESS=localhost:11051
```

**Invoke - CreateCars**
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["CarContract:CreateCar","Car-01", "Tata", "Tiago", "Blue", "Factory-1", "22/09/2023" ]}'
```
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["CarContract:CreateCar","Car-02", "Tata", "Tiago", "Blue", "Factory-1", "22/09/2023" ]}'
```
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["CarContract:CreateCar","Car-03", "Tata", "Tiago", "Blue", "Factory-1", "22/09/2023" ]}'
```

**Use the Queries**

```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsByRange", "Car-01", "Car-03"]}'
```

```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarHistory", "Car-01"]}'
```

```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["CarContract:DeleteCar","Car-01"]}'
```

```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarHistory", "Car-01"]}'
```
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsByRange", "Car-03", ""]}'
```
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsByRange", "", "Car-03"]}'
```
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsByRange", "", ""]}'
```

**To call from Org3**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsByRange", "", ""]}' --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT
```
**Get All Cars**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetAllCars"]}'
```
**Get Car with Pagination**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarsWithPagination", "10", ""]}'
```

**Index Folder**
```
mkdir -p META-INF/statedb/couchdb/indexes
```

**Get All Orders**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["OrderContract:GetAllOrders"]}'
```

**Get Orders by Range**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["OrderContract:GetOrdersByRange","",""]}'
```

## Final Flow

**Create Car - Org1 (Manufacturer)**
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["CarContract:CreateCar","CAR01", "Tata", "Tiago", "Black", "Factory-1", "22/09/2023" ]}'
```

**Create Order - Org2 (Dealer)**
```
export MAKE=$(echo -n "Tata" | base64 | tr -d \\n)
export MODEL=$(echo -n "Tiago" | base64 | tr -d \\n)
export COLOR=$(echo -n "Black" | base64 | tr -d \\n)
export DEALER_NAME=$(echo -n "Popular" | base64 | tr -d \\n)
```
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"Args":["OrderContract:CreateOrder","ORD01"]}' --transient "{\"make\":\"$MAKE\",\"model\":\"$MODEL\",\"color\":\"$COLOR\",\"dealerName\":\"$DEALER_NAME\"}"
```

**Query Match Orderers - Org1 (Manufacturer)**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["GetMatchingOrders", "CAR01"]}'
```
**Invoke Match orderers - Org1 (Manufacturer)**
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"function":"MatchOrder","Args":["CAR01","ORD01"]}'
```

**Register car as Org3 (MVD)**
```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C autochannel -n KBA-Automobile --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_PEER_TLSROOTCERT -c '{"function":"RegisterCar","Args":["CAR01","Sumi","KL-01-7777"]}'
```

**Query History - Org1 Org2**
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarHistory", "Car-01"]}'
```

**Query History - Org3** 
```
peer chaincode query -C autochannel -n KBA-Automobile -c '{"Args":["CarContract:GetCarHistory", "Car-01"]}' --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_PEER_TLSROOTCERT
```