package main

import "fmt"
// import "errors"
import "github.com/go/divide"


func main() {
	printMe("hello")
	var result, reminder, err = divide.IntDivision(8,5)

	// if(err != nil) { // general design pattern in go
	// 	fmt.Println(err.Error())
	// } else if reminder==0 {
	// 	fmt.Printf("The result is %v", result)
	// } else {
	// 	fmt.Printf("The result is %v and remider is %v", result, reminder)
	// }

	switch {
	case err != nil:
		fmt.Println(err.Error())
		// panic()
	case reminder == 0:
		fmt.Printf("The result is %v", result)
	default:
		fmt.Printf("The result is %v and reminder is %v", result, reminder)
	}

	// fmt.Println(result, reminder)
	// fmt.Printf("The result is %v and remider is %v \n", result, reminder)
	// fmt.Printf("The result is %v and remider is %v", result, reminder)
}



func printMe(data string) {
	fmt.Println(data)
}

