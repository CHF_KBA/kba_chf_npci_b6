**Managing Docker Resources: Cleanup and Deletion Commands**

```
docker container prune
```

```
docker system prune
```

```
docker volume prune
```

```
docker network prune
```

**To check for containers**

```
docker ps -a
```

**To forcefully remove containers**

```
docker rm $(docker container ls -q) --force
```


**To check for volumes**

```
docker volume ls
```


**To delete the volumes**

```
docker volume rm $(docker volume ls -q)
```


**Install docker-compose**

```
sudo apt install docker-compose -y
```

```
docker-compose version
```


**Download fabric-samples**

```
curl -sSLO https://raw.githubusercontent.com/hyperledger/fabric/main/scripts/install-fabric.sh && chmod +x install-fabric.sh
```


```
./install-fabric.sh -f '2.5.4' -c '1.5.7'
```


```
sudo cp fabric-samples/bin/* /usr/local/bin
```


**Test Network commands**

```
cd fabric-samples/test-network/
```


```
./network.sh -h
```


```
./network.sh up createChannel
```


```
docker ps --format "table {{.Image}}\t{{.Ports}}\t{{.Names}}"
```


**Deploy Chaincode Command**

```
./network.sh deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-go -ccl go
```


**Set up Environment Variables**

`export PATH=${PWD}/../bin:$PATH`   (**Note**: Execute this command if you have not copied the fabric binaries to /usr/local/bin)

```
export FABRIC_CFG_PATH=$PWD/../config/

export CORE_PEER_TLS_ENABLED=true

export CORE_PEER_LOCALMSPID="Org1MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt

export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

export CORE_PEER_ADDRESS=localhost:7051
```


**Invoke Chaincode**

```
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" -C mychannel -n basic --peerAddresses localhost:7051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" --peerAddresses localhost:9051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" -c '{"function":"InitLedger","Args":[]}'
```


**Query chaincode**

```
peer chaincode query -C mychannel -n basic -c '{"Args":["GetAllAssets"]}'
```


```
peer chaincode query -C mychannel -n basic -c '{"function":"ReadAsset","Args":["asset5"]}'
```


**Bring down network**

```
./network.sh down
```


```
docker volume rm $(docker volume ls -q)
```



**Automobile Network**

**Start the network**

```
cd fabric-samples/test-network
```


```
./network.sh up createChannel -c autochannel -ca -s couchdb
```


**Bring up org3**

```
cd addOrg3
```


```
./addOrg3.sh up -c autochannel -ca -s couchdb
```


```
cd ..
```


**Tear down the network**

```
./network.sh down
```


```
docker volume rm $(docker volume ls -q)
```





