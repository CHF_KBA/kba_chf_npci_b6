**Assignment 1: Multi-Containers in Docker**

**Objective**: Familiarize with Docker and Docker-Compose by building a simple image that prints a message, extends its functionality, and sets up inter-container communication.

**Docker Image "hello"**

1. Create a Dockerfile with ubuntu:latest as the base image.
2. In the Dockerfile, write instructions to print "Hello" when the container runs.
3. Build this Dockerfile and name the resulting image "hello".

**Docker-Compose Configuration**

Create a docker-compose.yaml file to manage multiple services.

1. hello-service: Modify the Dockerfile so that instead of printing "Hello", it reads the contents of a file named message.txt and displays them. Use the "hello" image. This service should mount the current directory (or wherever message.txt resides) to read the file.
2. ping1: Use a lightweight image like busybox. The command for this service should ping the ping2 service.
3. ping2: Also use a lightweight image like alpine. The command for this service should ping the ping1 service.
4. Ensure both ping services can communicate with each other.

**Execution**

Use docker-compose up to start all services and observe the logs to:

1. Confirm the hello-service prints the content of message.txt.
2. Ensure ping1 and ping2 are successfully pinging each other.

**Cleanup**
- Stop the services using docker-compose down.
- Then, clear all stopped containers, unused networks, and dangling images.

**Documentation**
Prepare a report detailing:
1. Steps and commands used throughout the process.
2. The Dockerfile content and its modifications.
3. The structure of your docker-compose.yaml.
4. Screenshots of the container logs, showcasing the "hello" message output and successful pings between ping1 and ping2.

This assignment provides a mix of Docker basics with multi-container orchestration using Docker-Compose. 
Once the assignment is complete, create a document with screenshots of the results and briefly explain the steps as per your understanding. Please send its pdf file to chf.kba@iiitmk.ac.in  on or before **02-Nov-2023, 10.00am**.
