package main

import "fmt"

type car struct {
	number uint
	color string
	model string
	carTravel
}

type carTravel struct {
	mpl int
	pertrol int
}

type electricCar struct {
	number uint
	color string
	model string
	mpkwh int
	kwh int
}

func(c car) milesLeft() int {
	return c.mpl * c.pertrol
}

func(c electricCar) milesLeft() int {
	return c.kwh * c.mpkwh
}

type carEngine interface {
	milesLeft() int
}

func canMakeIT(e carEngine, miles int) {
	if miles <= e.milesLeft() {
		fmt.Println("You can make it there")
	} else {
		fmt.Println("You wont make it")
	}
}

func main6() {
	var mycar car = car{number: 23, color: "Blue", model: "BMW", carTravel: carTravel{mpl: 23, pertrol: 23}}
	fmt.Println(mycar.number, mycar.color, mycar.model, mycar.mpl, mycar.pertrol)
	fmt.Printf("The miles left is %v \n", mycar.milesLeft())
	canMakeIT(mycar, 530)

	var myeccar = electricCar{number: 24, color: "White", model: "CWA", mpkwh: 21, kwh: 23}
	canMakeIT(myeccar, 23)
}