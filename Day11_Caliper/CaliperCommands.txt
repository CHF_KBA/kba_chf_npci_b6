//Run the provided commands from the directory where fabric-samples is located.

cd fabric-samples/test-network/

./network.sh up createChannel

./network.sh deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-go/ -ccl go

cd ../../

mkdir -p caliper-workspace/networks caliper-workspace/benchmarks caliper-workspace/workload

cd caliper-workspace/

npm install --only=prod @hyperledger/caliper-cli@0.5.0

npx caliper bind --caliper-bind-sut fabric:2.4

touch networks/networkConfig.yaml

touch workload/readAsset.js

touch benchmarks/myAssetBenchmark.yaml

npx caliper launch manager --caliper-workspace ./ --caliper-networkconfig networks/networkConfig.yaml --caliper-benchconfig benchmarks/myAssetBenchmark.yaml --caliper-flow-only-test --caliper-fabric-gateway-enabled

cd ../fabric-samples/test-network

./network.sh down
