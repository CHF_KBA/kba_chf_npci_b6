**Assignment - 4**

Build a Fabric network for the pharmaceutical use-case with participants as **Producer**, **Supplier**, **Wholesaler** and **Retailer**. Copy the docker-compose files and configtx.yaml file to a word doc and keep the screenshots of containers that are up and running. Deploy the chaincode developed for the pharmaceutical use-case, take the screenshot of an invoke and query transaction output, and add them to word doc. Please send the pdf file to **chf.kba@iiitmk.ac.in**  on or before **09-Nov-2023, 10.00am**. 
