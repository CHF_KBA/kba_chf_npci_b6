package main

import (
	"encoding/json"
	"fmt"
)

// Defining a struct
type Person struct {
	Name    string `json:"name"` 
	Age     int    `json:"age"`
	Address string `json:"address"`
}

func main7() {
	// Marshaling: Go struct to JSON string
	p1 := Person{Name: "John", Age: 30, Address: "123 Main St"}
	jsonData, err := json.Marshal(p1)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonData)) // Output: {"name":"John","age":30,"address":"123 Main St"}

	// Unmarshaling: JSON string to Go struct
	jsonString := `{"Name":"Alice","age":25,"address":"456 Elm St"}`
	var p2 Person
	err = json.Unmarshal([]byte(jsonString), &p2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(p2) // Output: {Alice 25 456 Elm St}
}

