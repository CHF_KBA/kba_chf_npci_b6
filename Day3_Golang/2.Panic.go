package main

import (
	"fmt"
	"log"
)

func main22() {
	// Using panic
	// defer fmt.Println("This will be executed before the panic stops the program")
	// fmt.Println("Second")
	// panic("A panic occurred!")

	// Using log.Panicf
	defer fmt.Println("This will be executed before the panic stops the program")
	log.Panicf("A panic occurred with value: %d", 42)
}