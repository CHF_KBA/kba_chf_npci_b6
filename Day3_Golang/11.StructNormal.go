package main

import (
	"fmt"
)

type car struct {
	mpl int
	petrol int
}

type eCar struct {
	mpkwh int
	kwh int
}

func(e eCar) milesLeft() int {
	return e.kwh * e.mpkwh
}

func(c car) milesLeft() int {
	return c.mpl * c.petrol
}

type carEngine interface {
	milesLeft() int
}

func canMakeItorNot(e carEngine, miles int) {
	if miles <= e.milesLeft() {
		fmt.Println("Yes, you can make it")
	} else {
		fmt.Println("Nop")
	}
}

func main() {
	var myCar car = car{mpl: 23, petrol: 20}
	var myCar1 car = car{mpl: 4, petrol: 5}
	var electric eCar = eCar{kwh: 3, mpkwh: 5}
	fmt.Println(myCar.milesLeft())
	fmt.Println(myCar1.milesLeft())
	canMakeItorNot(electric, 25)
}