package divide

import "errors"

func IntDivision(numerator int, denomenator int) (int, int, error) {
	var err error

	if denomenator == 0 {
		err = errors.New("cannot divide by zero")
		// err = fmt.Errorf("cant use denomenator %v to divide numerator %v", denomenator, numerator)
		return 0, 0, err
	}
	var result int = numerator / denomenator
	var reminder int = numerator % denomenator
	return result, reminder, err
}