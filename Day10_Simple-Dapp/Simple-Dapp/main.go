package main

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
)

type Car struct {
	CarId        string `json:"carId"`
	Make         string `json:"make"`
	Model        string `json:"model"`
	Color        string `json:"color"`
	Date         string `json:"dateOfManufacture"`
	Manufacturer string `json:"manufacturerName"`
}

func main() {
	router := gin.Default()

	var wg sync.WaitGroup
	wg.Add(1)
	go ChaincodeEventListener("manufacturer", "autochannel", "KBA-Automobile", &wg)

	router.Static("/public", "./public")
	router.LoadHTMLGlob("templates/*")

	router.GET("/", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", gin.H{
			"title": "Auto App",
		})
	})

	router.POST("/api/car", func(ctx *gin.Context) {
		var req Car
		if err := ctx.BindJSON(&req); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": "Bad request"})
			return
		}

		fmt.Printf("car response %s", req)
		emptyPvtData := make(map[string][]byte)
		submitTxnFn("manufacturer", "autochannel", "KBA-Automobile", "CarContract", "invoke", emptyPvtData, "CreateCar", req.CarId, req.Make, req.Model, req.Color, req.Manufacturer, req.Date)

		ctx.JSON(http.StatusOK, req)
	})

	router.GET("/api/car/:id", func(ctx *gin.Context) {
		carId := ctx.Param("id")

		emptyPvtData := make(map[string][]byte)
		result := submitTxnFn("manufacturer", "autochannel", "KBA-Automobile", "CarContract", "query", emptyPvtData, "ReadCar", carId)

		ctx.JSON(http.StatusOK, gin.H{"data": result})
	})


	router.GET("/get-event", func(ctx *gin.Context) {
		result := getEvents()
		fmt.Println("result:", result)

		ctx.JSON(http.StatusOK, gin.H{"carEvent": result})
	})

	router.Run("0.0.0.0:3000")
}