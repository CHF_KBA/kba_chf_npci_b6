**Assignment - 3**

Build a Fabric test-network for a pharmaceutical use-case with three organizations. If you are familiar with Minifab, then create minifab network with four organizations like Producer, Supplier, Wholesaler and Retailer.

Build a chaincode and implement functions to
- Add medicines with properties: medicine name, quantity, date of manufacture, date of expiry, owner etc.
- Delete the medicine
- List out the medicines in ascending order of ‘medicine name’
- Show the history of medicines
- Implement a function to request a medicine such that the details of the requests should be available with only two organizations

Copy the smart contract to a word document and also add the screenshots of following output:

- Add about 10 medicines and list out in the ascending order of ‘medicine name’. 
- Delete a medicine and show the history of that medicine. 
- Create requests by setting the details of request as transient field.

Please send the pdf file to chf.kba@iiitmk.ac.in  on or before **7-Nov-2023, 10.00am**.
