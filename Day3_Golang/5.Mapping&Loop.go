package main

import "fmt"

// key -> value

func main5() {
	// var myMap map[string]uint = make(map[string]uint)
	// fmt.Println(myMap)
	// myMap2 := map[string]string {"Eve":"23","Alen":"12", "Alex":"20"}
	// fmt.Println(myMap2)
	// fmt.Println(myMap2["Alen"])
	// fmt.Println(myMap2["Ananthan"]) // will return default value of value type, so be carefull when utilizing it
	// var age, exisit = myMap2["Alex"] // maps return a bool indicating value exisit or not
	// // fmt.Println(age,exisit)
	// if(exisit) {
	// 	fmt.Println(age)
	// }
	// delete(myMap2, "Alen")
	// fmt.Println(myMap2)
	// for name, age := range myMap2 { // same for array or slices
	// 	fmt.Printf("Name %v and %v \n", name, age)
	// }

	arr := []int{1,2,3,4,5}

	for i, v := range arr {
		fmt.Printf("Index %v and value %v \n", i,v)
	}

	// while loop in go
	var i int = 0
	for i<10 {
		fmt.Println(i)
		i = i+1
	}

	for {
		if(i>=10) {
			break
		}
		fmt.Println(i)
		i = i+1
	}

	for i:=0; i<10; i++ {
		fmt.Println(i)
	}
}