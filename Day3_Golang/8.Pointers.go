package main

import "fmt"

// pointers are a special type of variables
func main8() {
	var p *int // variable p will holde the address of an int variable
	var i int = 2

	p = &i

	fmt.Println(*p)

	// p = &i
	// i = 1
	// *p = 2

	// fmt.Println(p)
	// fmt.Println(*p)
	// fmt.Println(i)

	// var np *int = new(int)
	// fmt.Println(np)
}