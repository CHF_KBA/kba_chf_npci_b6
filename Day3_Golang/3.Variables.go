package main

// import "fmt"

func main33() {
	
	// fmt.Print("Hello World this is Go...! ")
	// fmt.Println("Hello World this is Go...!")

	// var intNumber int = 12

	// // intNumber = intNumber + 1;

	// fmt.Println(intNumber)
	
	// var data int8 = 127 // if assign 128 it will show an error basically overflow, rang -128 - 127
	// data = data + 2 // this wont be a compile error raher a run weird output

	// var data uint8 = 127
	// data = data + 2 // here it works as its unsigned integer so now the range is 0 - 255

	// data, data1 := 127, "string" // short hand, type infered
	// fmt.Println(data, data1)
	
	// data := "hai"
	// number := 21
	// fmt.Println(data, number)
	// fmt.Println(data + number)

	// 	data := `hello 
	// world` // format string using `

	// data:= "Hello" + " " + "World"
	// fmt.Println(data)
	// fmt.Println(len("γ")) // >> 2 bcz its the nouber of bytes, go use UTF8 so char outside  “vanilla” ASCII are stored with more than single byte

	// const pi float64 = 322222.14 // need to be initialized
	// fmt.Println(pi) // gets inaccurate value so try chaninging to float64
} 