**Assignment - 2**

As part of project submission, select a use case, where applying distributed ledger is relevant. Create a simple pitch deck showing the details and workflow of your project. A sample template for the pitch deck is attached. Once you have prepared the pitch deck, please send the pdf file to chf.kba@iiitmk.ac.in  on or before **06-Nov-2023**, **10.00am**. 

Sample Case Studies and Use cases for reference:


https://www.hyperledger.org/case-studies

https://www.hyperledger.org/learn/publications/dltlabs-case-study

https://www.hyperledger.org/learn/publications/openidl-case-study

https://www.hyperledger.org/learn/publications/joitso-solution-brief

https://www.hyperledger.org/learn/case-studies/mindtree-case-study

https://www.hyperledger.org/news/2018/08/01/8-18-18-ledger-insights-monetagos-blockchain-solution-for-trade-finance-fraud

https://we-trade.com/

https://www.ibm.com/blockchain/industries/financial-services
