package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main11() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Enter your name:")
	name, _ := reader.ReadString('\n')

	// Trim the newline character from the end of the name
	name = strings.TrimSpace(name)

	fmt.Println("Name:", name)
}
