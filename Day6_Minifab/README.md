**Install Minifab**

`curl -o minifab -sL https://tinyurl.com/yxa2q6yr && chmod +x minifab`

`sudo cp minifab /usr/local/bin`

`minifab`

**Minifab**

https://github.com/hyperledger-labs/minifabric

https://github.com/hyperledger-labs/minifabric/blob/main/spec.yaml

**Bring up the network**

**Note**: Execute the following commands from **MinifabNetwork** folder, where the **spec.yaml** file is available.

`minifab netup -s couchdb -e true -i 2.4.8 -o manufacturer.auto.com`

`minifab create -c autochannel`

`minifab join -c autochannel`

`minifab anchorupdate`

**Bring down the network**

`minifab cleanup`

**Using script**

`chmod +x startNetwork.sh`

`./startNetwork.sh`

**Minifab commands to deploy and invoke chaincode (If PDC is used)**

`./startNetwork.sh`

`sudo chmod -R 777 vars/`

`mkdir -p vars/chaincode/KBA-Automobile/go`

`cp -r ../Chaincode/* vars/chaincode/KBA-Automobile/go/`

`cp vars/chaincode/KBA-Automobile/go/collection-minifab.json ./vars/KBA-Automobile_collection_config.json`

`minifab ccup -n KBA-Automobile -l go -v 1.0 -d false -r true`

`minifab invoke -n KBA-Automobile -p '"CreateCar","car01","Tata","Tiago","White","F-01","22/03/2023"'`

`minifab query -n KBA-Automobile -p '"ReadCar","car01"'`

```
MAKE=$(echo -n "Tata" | base64 | tr -d \\n)
MODEL=$(echo -n "Tiago" | base64 | tr -d \\n)
COLOR=$(echo -n "White" | base64 | tr -d \\n)
DEALER_NAME=$(echo -n "XXX" | base64 | tr -d \\n)
```

`minifab invoke -n KBA-Automobile -p '"OrderContract:CreateOrder","ord01"' -t '{"make":"'$MAKE'","model":"'$MODEL'","color":"'$COLOR'","dealerName":"'$DEALER_NAME'"}' -o dealer.auto.com`

`minifab query -n KBA-Automobile -p '"OrderContract:ReadOrder","ord01"'`

`minifab query -n KBA-Automobile -p '"GetAllCars"'`

`minifab query -n KBA-Automobile -p '"GetCarsByRange","car01","car05"'`

`minifab query -n KBA-Automobile -p '"OrderContract:GetAllOrders"'`

`minifab query -n KBA-Automobile -p '"OrderContract:GetOrdersByRange","ord01","ord05"'`

`minifab query -n KBA-Automobile -p '"GetCarHistory","car01"'`

`minifab query -n KBA-Automobile -p '"GetCarsWithPagination","10",""'`

`minifab query -n KBA-Automobile -p '"GetMatchingOrders","car01"'`

`minifab invoke -n KBA-Automobile -p '"MatchOrder","car01","ord01"'`

`minifab invoke -n KBA-Automobile -p '"RegisterCar","car01","Bob","KL-01-XXXX"' -o mvd.auto.com`

**Update the chaincode**

`cp -r ../Chaincode/* vars/chaincode/KBA-Automobile/go/`

`cp vars/chaincode/KBA-Automobile/go/collection-minifab.json ./vars/KBA-Automobile_collection_config.json`

`minifab ccup -n KBA-Automobile -l go -v 2.0 -d false -r true`

**To view explorer**

`minifab explorerup`

userid: exploreradmin

password: exploreradminpw

`minifab explorerdown`

**To view couchdb**

http://localhost:7006/_utils/

 userid: `admin`
 
 password: `adminpw`

**To view the details of a block**

`minifab blockquery`

`minifab blockquery -b 6`


**To view blockchain**

`docker exec -it peer1.mvd.auto.com /bin/sh`

`ls var/hyperledger/production/ledgersData/chains/chains/autochannel/`

`cat var/hyperledger/production/ledgersData/chains/chains/autochannel/blockfile_000000`

`exit`

**To stop network and restart it later**

`minifab down`

`minifab restart`

**To cleanup entire network**

`minifab cleanup`

`sudo rm -rf vars`

