
## Clean Up
To list containers use
```
docker ps -a
```
`-a` for showing terminated containers also.

To remove a specific container
```
docker rm <container_id_or_name>
```
Incase its still running use, stop it before removing
```
docker stop <container_id_or_name>
docker rm <container_id_or_name>
```

Or use prune to remove all the stopped containers
```
docker container prune
```

Now images, to list them use
```
docker images
```
To remove a specific image
```
docker rmi <image_id_or_name:tag>
```
Now  the dangling images, those images that are not tagged and are not used by any container. To remove them:
```
docker image prune
```
To remove all images use
```
docker rmi $(docker images -q)
```

Now for volumes, to list them use
```
docker volume ls
```

To remove them
```
docker volume rm [volume_name]
```

To remove all volumes
```
docker volume rm $(docker volume ls -q)
```

To remove unused images, use
```
docker volume prune
```

To get voulme details, use 
```
docker volume inspect [volume_name]
```